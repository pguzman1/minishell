/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv_builtin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 14:55:16 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 09:37:48 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_var_exists(char **cp, const char *name, int len)
{
	int		i;

	i = 0;
	if (!cp)
		return (-2);
	while (i < len)
	{
		if (ft_strcmp(name, cp[i]) == -61)
			return (i);
		i++;
	}
	return (-2);
}

void	ft_puttab(char **table)
{
	int i;

	i = 0;
	while (table[i])
	{
		ft_putendl(table[i]);
		i++;
	}
}

int		ft_setenv_error(char **s)
{
	if (s[3] && s[2] && s[1] && s[0])
	{
		ft_putstr_fd("setenv: Too many arguments.\n", 2);
		return (1);
	}
	if (!ft_isalpha(s[1][0]))
	{
		ft_putstr_fd("setenv: Variable name must begin with a letter.\n", 2);
		return (1);
	}
	if (!ft_wd_isalnum(s[1]))
	{
		ft_putstr_fd("setenv: Variable ", 2);
		ft_putstr_fd("name must contain alphanumeric characters.\n", 2);
		return (1);
	}
	if (ft_strchr(s[1], '='))
	{
		ft_putstr_fd("setenv: Syntax Error.\n", 2);
		return (1);
	}
	return (0);
}

int		setenv_bi(char **s, char ***cp)
{
	char	*st;
	int		i;
	int		len;
	char	*d;

	len = ft_envlen(*cp);
	if (s[1] == NULL)
		return (env_bi(s, cp));
	if (ft_setenv_error(s) == 1)
		return (1);
	d = ft_strjoin(s[1], "=");
	st = s[2] ? ft_strjoin(d, s[2]) : d;
	s[2] ? ft_strdel(&d) : NULL;
	i = ft_var_exists(*cp, s[1], len);
	if (i == -2)
		*cp = ft_add_var(*cp, st);
	else
	{
		d = (*cp)[i];
		(*cp)[i] = st;
		ft_strdel(&d);
	}
	return (1);
}
