/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_copy.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 15:29:02 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/02 13:53:42 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**ft_env_copy(char *envp[])
{
	char	**cp;
	int		len;
	int		i;

	i = 0;
	len = ft_envlen(envp);
	cp = (char **)malloc(sizeof(*cp) * (len + 3));
	while (i < len)
	{
		cp[i] = ft_strdup(envp[i]);
		i++;
	}
	cp[i] = NULL;
	return (cp);
}

char	**ft_add_var(char **cp, char *str)
{
	char	**res;
	int		len;
	int		i;

	i = 0;
	len = ft_envlen(cp);
	res = (char **)malloc(sizeof(*res) * (len + 2));
	while (i < len)
	{
		res[i] = ft_strdup(cp[i]);
		i++;
	}
	res[i++] = ft_strdup(str);
	res[i] = NULL;
	ft_tabdel(&cp);
	return (res);
}

char	**ft_new_tab(void)
{
	char	**res;

	res = (char **)malloc(sizeof(*res) * (1));
	res[1] = (char *)malloc(sizeof(res[1]) * 1);
	res[2] = NULL;
	return (res);
}
