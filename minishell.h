/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 11:05:50 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 14:25:44 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>

typedef struct	s_all
{
	int		status;
	char	*line;
	char	**s;
	char	*builtin[5];
	char	**cp;
}				t_all;

int		ft_launch(char **s, char **cp);

int		cd_bi(char **s, char ***cp);
int		setenv_bi(char **s, char ***cp);
int		ft_envlen(char **envp);

char	**ft_env_copy(char *envp[]);
int		ft_var_exists(char **cp, const char *name, int len);
char	**ft_add_var(char **cp, char *str);

int		env_bi(char **s, char ***cp);
int		unsetenv_bi(char **s, char ***cp);
void	sig_handler(int sign);
int		ft_char_in_str(char *s, char c);
int		ft_wd_isalnum(char *s);

int		ft_sig(void);
int		ft_builtins(char **s, char ***cp, char *builtin[5]);
char	**ft_tab_trim(char **s);
int		ft_isblanck(char c);
char	**ft_strsplit2(char const *s);
char	**ft_new_tab();
void	ft_init(int *argc, char ***argv, t_all *a, char **envp);
int		ft_cmd_not_found(char **s, char ***cp, char *builtin[5]);
void	ft_init_builtin(char *builtin[]);
char	**ft_path_i(void);
char	**ft_get_path(char **cp);
int		ft_isnumber(char *s);
int		ft_isdir(char *s);
void	ft_tabdel(char ***tb);
char	*get_env(const char *name, char **cp);
