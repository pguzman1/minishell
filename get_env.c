/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_env.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 09:38:36 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/02 15:02:27 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*get_env(const char *name, char **cp)
{
	int		i;

	i = 0;
	if (!cp)
		return (NULL);
	while (cp[i])
	{
		if (ft_strcmp(name, cp[i]) == -61)
			return (ft_strchr(cp[i], '=') + 1);
		i++;
	}
	return (NULL);
}
