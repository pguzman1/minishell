/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd_builtin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 13:57:38 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 11:29:56 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_change_pwd(char ***cp, char *newpwd, char *oldpwd)
{
	int		i;
	char	*newvar;
	char	*oldvar;
	int		len;

	len = ft_envlen(*cp);
	i = 0;
	oldvar = ft_strjoin("OLDPWD=", oldpwd);
	newvar = ft_strjoin("PWD=", newpwd);
	(*cp)[ft_var_exists(*cp, "PWD", len)] = newvar;
	(*cp)[ft_var_exists(*cp, "OLDPWD", len)] = oldvar;
}

void	ft_print_cd_error(char *s)
{
	if (access(s, F_OK) != 0)
		ft_putstr_fd("cd : no such file or directory\n", 2);
	else if (!ft_isdir(s))
		ft_putstr_fd("cd : not a directory\n", 2);
	else if (access(s, X_OK) != 0)
	{
		ft_putstr_fd("cd: permission denied: ", 2);
		ft_putstr_fd(s, 2);
		ft_putstr_fd("\n", 2);
	}
}

void	ft_mov_and_cwd(char *str, char ***cp, char *str2)
{
	chdir(get_env(str, *cp));
	ft_change_pwd(cp, get_env(str, *cp), str2);
}

void	ft_cd_core(char **s, char ***cp, int i)
{
	char	old[1024];
	char	new[1024];

	getcwd(old, sizeof(old));
	if (s[i])
		i = (ft_strcmp("--", s[i]) == 0) ? i + 1 : i;
	if (!s[i])
		ft_mov_and_cwd("HOME", cp, old);
	else if (ft_strcmp(s[i], "~") == 0)
		ft_mov_and_cwd("HOME", cp, old);
	else if (ft_strcmp(s[i], "-") == 0)
		ft_mov_and_cwd("OLDPWD", cp, old);
	else if (chdir(s[i]) != 0)
		ft_print_cd_error(s[i]);
	else
	{
		getcwd(new, sizeof(new));
		ft_change_pwd(cp, new, old);
	}
}

int		cd_bi(char **s, char ***cp)
{
	int		i;

	i = 1;
	if (get_env("HOME", *cp) == NULL && !s[1])
	{
		ft_putstr_fd("/usr/bin/cd: line 4: cd: HOME not set\n", 2);
		return (1);
	}
	else if (!get_env("PWD", *cp))
		return (1);
	ft_cd_core(s, cp, i);
	return (1);
}
