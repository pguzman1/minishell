/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extras.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/09 12:47:39 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 14:25:38 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_char_in_str(char *s, char c)
{
	int		i;
	int		j;

	j = 0;
	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			j++;
		i++;
	}
	return (j);
}

int		ft_wd_isalnum(char *s)
{
	int		i;

	i = 0;
	while (s[i])
	{
		if (!ft_isalnum(s[i]) && (s[i] != '_'))
			return (0);
		i++;
	}
	return (1);
}

int		ft_isdir(char *s)
{
	struct stat *buf;

	buf = malloc(sizeof(struct stat));
	lstat(s, buf);
	if (S_ISDIR(buf->st_mode))
	{
		free(buf);
		return (1);
	}
	else
	{
		free(buf);
		return (0);
	}
}

int		ft_isnumber(char *s)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (!ft_isdigit(s[i]))
			return (0);
		i++;
	}
	return (1);
}
void	ft_tabdel(char ***tb)
{
	long	i;

	if (*tb && **tb)
	{
		i = 0;
		while ((*tb)[i] != NULL)
		{
			free((*tb)[i]);
			++i;
		}
		free(*tb);
		*tb = NULL;
	}
}
