/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv_builtin.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 18:48:49 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 09:36:18 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		unsetenv_bi(char **s, char ***cp)
{
	int	len;
	int	i;
	int	j;

	j = 0;
	i = -2;
	if (!s[1])
		ft_putstr_fd("unsetenv: Too few arguments.\n", 2);
	while (s[j + 1])
	{
		len = ft_envlen(*cp);
		i = ft_var_exists(*cp, s[j + 1], len);
		if (i != -2)
		{
			while (i < len - 1)
			{
				(*cp)[i] = (*cp)[i + 1];
				i++;
			}
			(*cp)[i] = NULL;
		}
		j++;
	}
	return (1);
}
