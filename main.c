/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 11:02:05 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 14:25:34 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_child(char **s, char **cp)
{
	char	**split;
	int		i;
	char	*temp;
	char	*del;

	i = -1;
	split = (!(get_env("PATH", cp))) ? (ft_path_i()) : (ft_get_path(cp));
	while (split[++i])
	{
		if (s[0][0] == '.' || s[0][0] == '/')
			temp = s[0];
		else
		{
			del = ft_strjoin("/", s[0]);
			temp = ft_strjoin(split[i], del);
			free(del);
		}
		if (execve(temp, s, cp) != -1)
		{
			free(temp);
			return (1);
		}
		free(temp);
	}
	return (0);
}

int		ft_launch(char **s, char **cp)
{
	pid_t	pid;
	int		i;

	pid = fork();
	if (pid > 0)
	{
		wait(&pid);
		return (1);
	}
	if (pid == 0)
	{
		i = ft_child(s, cp);
		if (i == 0)
			return (0);
		return (1);
	}
	return (1);
}

int		ft_builtins(char **s, char ***cp, char *builtin[5])
{
	int		status;

	status = -2;
	status = (ft_strcmp(s[0], builtin[0]) == 0) ? cd_bi(s, cp) : status;
	status = (ft_strcmp(s[0], builtin[1]) == 0) ? setenv_bi(s, cp) : status;
	status = (ft_strcmp(s[0], builtin[2]) == 0) ? unsetenv_bi(s, cp) : status;
	status = (ft_strcmp(s[0], builtin[3]) == 0) ? env_bi(s, cp) : status;
	status = (ft_strcmp(s[0], builtin[4]) == 0) ? 0 : status;
	if (status == -2)
		status = ft_launch(s, *cp) == 1 ? 1 : ft_cmd_not_found(s, cp, builtin);
	return (status);
}

int		ft_sig(void)
{
	int	status;

	status = 1;
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		status = 1;
	if (signal(SIGQUIT, sig_handler) == SIG_ERR)
		status = 1;
	if (signal(SIGTSTP, sig_handler) == SIG_ERR)
		status = 1;
	return (status);
}

char	**ft_parsing(char *line)
{
	char	**s;

	line = ft_strtrim(line);
	s = ft_strsplit2(line);
	free(line);
	return (s);
}

int		ft_exit_error(t_all *a)
{
	if (!(a->status) && !(a->s)[1])
		return (0);
	if (!(a->status) && (a->s)[2])
	{
		a->status = 1;
		ft_putstr_fd("exit: Expression Syntax.\n", 2);
		return (1);
	}
	else if (!(a->status) && ft_isnumber(s[1]))
		return (0);
	else
	{
		*status = 1;
		ft_putstr_fd("exit: Expression Syntax.\n", 2);
		return (0);
	}

}

int		main(int argc, char **argv, char *envp[])
{
	t_all	a;

	ft_init(&argc, &argv, &a, envp);
	while (a.status)
	{
		ft_putchar('>');
		if (!get_next_line(0, &a.line))
		{
			ft_putstr("exit\nexit\n");
			return (0);
		}
		a.s = ft_parsing(a.line);
		a.status = a.s[0] ? ft_builtins(a.s, &a.cp, a.builtin) : a.status;
		ft_tabdel(&a.s);
		if (a.status == 0)
		{
			if (ft_exit_error(&a) == 0)
			{
				return (ft_atoi((a.s)[1]));	
			}

		}
	}
	ft_tabdel(&a.cp);
	return (0);
}
