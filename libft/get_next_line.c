/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 15:44:20 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/09 09:21:46 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_strchr_i(char *s, char c)
{
	int				i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

static int		ft_get_line(char **str, char **line, char **temp, int *ret)
{
	ft_strdel(temp);
	if (ft_strchr(*str, '\n'))
	{
		if ((*line = ft_strsub(*str, 0, ft_strchr_i(*str, '\n'))) == NULL)
			return (-1);
		*temp = *str;
		if ((*str = ft_strdup(*str + ft_strchr_i(*str, '\n') + 1)) == NULL)
			return (-1);
		ft_strdel(temp);
		return (1);
	}
	if (*ret <= 0)
	{
		if ((*line = ft_strdup(*str)) == NULL)
			return (-1);
		ft_strdel(str);
		return (*line[0] != '\0') ? (1) : (*ret);
	}
	return (2);
}

int				get_next_line(int const fd, char **line)
{
	char			buf[BUFF_SIZE + 1];
	static char		*str = NULL;
	int				ret;
	char			*temp;
	int				res;

	if (fd < 0 || line == NULL)
		return (-1);
	while ((str = (!str) ? (ft_strnew(1)) : (str)))
	{
		ret = read(fd, buf, BUFF_SIZE);
		buf[ret] = '\0';
		temp = str;
		if ((str = (ret >= 0) ? ft_strjoin(str, buf) : (str)) == NULL)
			return (-1);
		if ((res = ft_get_line(&str, line, &temp, &ret)) != 2)
			return (res);
	}
	return (-1);
}
