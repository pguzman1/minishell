/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 10:04:55 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/01 11:05:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *lst;
	t_list *temp;

	lst = *alst;
	temp = lst;
	while (temp)
	{
		lst = lst->next;
		del(temp, 1);
		temp = lst;
	}
	*alst = NULL;
}
