/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:09:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/13 16:41:58 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;
	char	*res;
	char	temp;

	temp = (char)c;
	res = (char *)s;
	i = 0;
	if (temp == '\0')
		return (res + ft_strlen(s));
	while (s[i])
	{
		if (s[i] == temp)
			return (res + i);
		i++;
	}
	return (NULL);
}
