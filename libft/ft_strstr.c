/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:13:22 by pguzman           #+#    #+#             */
/*   Updated: 2015/11/25 15:45:29 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		j;
	char	*temp;

	temp = (char *)s1;
	i = 0;
	if (s2[0] == '\0')
		return (temp);
	while (s1[i])
	{
		j = 0;
		if (s1[i + j] == s2[j])
		{
			while (s1[i + j] == s2[j])
			{
				if (s2[j + 1] == '\0')
				{
					return (&temp[i]);
				}
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
