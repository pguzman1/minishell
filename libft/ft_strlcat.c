/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 13:30:13 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/01 09:28:34 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	len;

	len = ft_strlen(dst) + ft_strlen(src);
	if (size > ft_strlen(dst))
	{
		ft_strncat(dst, src, size - ft_strlen(dst) - 1);
		return (len);
	}
	else
	{
		return (len - (ft_strlen(dst) - size));
	}
}
