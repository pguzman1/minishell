/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:05:13 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/16 15:22:29 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strtrim(char const *s)
{
	int		beg;
	int		end;

	end = ft_strlen(s);
	beg = 0;
	if (end == beg)
		return (ft_strdup(""));
	while (s[beg] == '\n' || s[beg] == ' ' || s[beg] == '\t')
		beg++;
	while (s[end] == '\n' || s[end] == ' ' || s[end] == '\t' || s[end] == '\0')
		end--;
	if (beg > end)
		return (ft_strdup(""));
	return (ft_strsub(s, beg, end - beg + 1));
}
