/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 14:58:48 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 14:26:38 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**ft_get_path(char **cp)
{
	char	**split;

	split = ft_strsplit(ft_strchr(cp[ft_var_exists(cp, "PATH", \
					ft_envlen(cp))], '=') + 1, ':');
	return (split);
}

char	**ft_path_i(void)
{
	char	**split;

	split = malloc(sizeof(*split) * 10);
	split[0] = "/usr/bin";
	split[1] = "/bin";
	split[2] = NULL;
	return (split);
}

int		ft_cmd_not_found(char **s, char ***cp, char *builtin[5])
{
	char	**tmp;

	tmp = *cp;
	if (ft_strcmp(builtin[1], "") == 0)
	{
		ft_putstr_fd("env: ", 2);
		ft_putstr_fd(s[0], 2);
		ft_putstr_fd(": No such file or directory\n", 2);
		return (0);
	}
	ft_putstr_fd(s[0], 2);
	ft_putstr_fd(": Command not found.\n", 2);
	return (0);
}

void	ft_init(int *argc, char ***argv, t_all *a, char **envp)
{
	*argc = 0;
	*argv = NULL;
	(*a).status = 1;
	(*a).line = NULL;
	(*a).builtin[0] = "cd";
	(*a).builtin[1] = "setenv";
	(*a).builtin[2] = "unsetenv";
	(*a).builtin[3] = "env";
	(*a).builtin[4] = "exit";
	(*a).cp = ft_env_copy(envp);
	(*a).status = ft_sig();
}
