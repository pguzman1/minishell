# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pguzman <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/08 11:16:06 by pguzman           #+#    #+#              #
#    Updated: 2016/03/02 15:02:18 by pguzman          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= minishell

SRC		= main.c cd_builtin.c tools.c env_copy.c setenv_builtin.c env_builtin.c\
		  unsetenv_builtin.c exit_builtin.c signal.c extras.c get_env.c trim.c \
		  ft_strsplit2.c ft_isblanck.c main2.c

OBJ		= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -Werror

$(NAME): $(OBJ)
		@make -C libft/
			@gcc $(OBJ) -lncurses -o $(NAME) -L libft/ -lft

all: $(NAME)

clean:
		@make -C libft/ clean
			@rm -rf $(OBJ)

fclean: clean
		@rm -rf $(NAME) $(OBJ)

re: fclean $(NAME)
