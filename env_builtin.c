/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_builtin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 15:04:15 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/03 11:01:42 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_init_env_i(char *builtin[5])
{
	builtin[0] = "cd";
	builtin[1] = "";
	builtin[2] = "";
	builtin[3] = "env";
	builtin[4] = "";
}

void	ft_print_tab(char **tb)
{
	int i;

	i = 0;
	if (tb != NULL)
		while (tb[i])
			ft_putendl(tb[i++]);
}

int		ft_env_opt(char *s)
{
	if (s[0] == '-' && s[1])
	{
		if (ft_strcmp(s, "-i") != 0)
		{
			ft_putstr_fd("env: illegal option -- ", 2);
			if (s[1] != 'i')
				ft_putchar(s[1]);
			else
				ft_putchar(s[2]);
			ft_putstr_fd("\n", 2);
			ft_putstr_fd("usage: env [-iv] [-P utilpath] [-S string] [-u name]\n\
		[name=value ...] [utility [argument ...]]\n", 2);
			return (-1);
		}
	}
	return (1);
}

int		env_bi(char **s, char ***cp)
{
	int		i;
	char	**tmp;
	char	*builtin[5];

	i = 0;
	ft_init_env_i(builtin);
	if (!s[1])
		ft_print_tab(*cp);
	else
	{
		tmp = (ft_strcmp(s[1], "-i") == 0) ? NULL : *cp;
		i = (ft_strcmp(s[1], "-i") == 0) ? 1 : 0;
		if (ft_env_opt(s[1]) == -1)
			return (1);
		while (s[1 + i])
		{
			if (ft_strrchr(s[1 + i], '='))
				tmp = ft_add_var(tmp, s[1 + i++]);
			else
				return (ft_builtins(&s[1 + i], &tmp, builtin));
		}
		ft_print_tab(tmp);
	}
	return (1);
}
